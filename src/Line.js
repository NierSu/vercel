import { useRef, useEffect, useState } from 'react'
import Typed from 'typed.js'
import PopupImages from './PopupImages';
import './Line.css'

function Line(props) {
    const typedRef = useRef(null)

    const [show, setShow] = useState(false)
    const [cn, setcn] = useState("fadeIn")

    useEffect(() => {
        const typed = new Typed(typedRef.current, {
            strings: props.strings,
            backDelay: 1000,
            typeSpeed: 100,
            showCursor: false,
            onBegin: () => {
                setShow(!show)
            },
            onComplete: (self) => {
                
                typedRef.current.className = "fadeOut"
                setcn("fadeOut")
                setTimeout(() => {
                    setShow(!show)
                    props.setDestroy(false)
                    if(props.rolling){
                        props.rolling(true)
                    }
                    
                }, 2900)
            },
        });
        return () => {
            console.log("fired")
            typed.destroy();
        };
    }, []);

    return (

        <div>
            {props.children(typedRef, { ...props.customStyle, color: "white", position: "absolute", fontSize: 40 })}
            {
                show &&
                <PopupImages cn={cn} image={props.image} customStyle={props.customImageStyle} />
           }
           
        </div>

    )
}

export default Line