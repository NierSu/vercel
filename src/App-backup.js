import { useState } from 'react';
import { Button } from 'antd';
import Paral from './Parallax';
import Baobao from './Baobao';
import legs from './img/legs.png';
//import Test from './Test';
import './App.css';



function App() {
  const [x, setx] = useState(0);
  const [y, sety] = useState(0);
  
  function handleDragOver(e) {
    e.preventDefault();
  }

  function handleDrop(e) {
    e.preventDefault();
    // console.log(typeof e.clientX, typeof e.dataTransfer.getData('x'));
    setx(e.clientX - parseFloat(e.dataTransfer.getData('x')));
    sety(e.clientY - parseFloat(e.dataTransfer.getData('y')));
  }

  return (
    <div onDragOver={handleDragOver} onDrop={handleDrop}>
      
      <Paral />
      <Baobao x={x} y={y} />
      <img style={{position:"absolute", left:"50%", top:"50%", transform:"translate(-50%, -50%)", zIndex:5}} src={legs} alt="legs" />
    </div>
  )
};



export default App;








