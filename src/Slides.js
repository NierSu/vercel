import './Slides.css';
import { useRef, useEffect } from 'react'
import Typed from 'typed.js'

function Slides(props) {
    const typedRef = useRef(null)
    useEffect(() => {
            console.log("mounting and unmounting")
            const typed = new Typed(typedRef.current, {
                strings: ["Long long time ago....", "There was a princess whose name was 静茹.... "],

                startDelay: 2000,
                backDelay: 1000,
                typeSpeed: 100,
                showCursor: false,
                onComplete: (self) => {
                    console.log('first line complete')
                    props.setStage(1)
                    typedRef.current.className = "fadeOut"
                    setTimeout(() => {
                        props.setLegging(true)
                        console.log("first line unmounting begins")
                        props.showNext(true)
                    }, 2900)
                },
            });
        
        return () => {
            console.log("first line unmounted")
            typed.destroy();
        };
    }, []);

    return (

        <div>
            {props.children(typedRef)}
        </div>

    )
}

export default Slides