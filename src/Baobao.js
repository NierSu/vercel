import React from 'react'
import { useEffect, useRef, useState } from 'react';
import './Baobao.css';
//import { DragDropContext, Droppable, Draggable } from 'react-beautiful-dnd';


import baobaoImg from './img/baobao.png';
import heart from './img/heart.png';

function Baobao(props) {
    const [show, setShow] = useState(false);

    const baobaoRef = useRef(null);
    const baobaoContainer = useRef(null);

    function timming() {

        setShow(false);

    }

    function handleDragStart(e) {
        const x = e.nativeEvent.offsetX;
        const y = e.nativeEvent.offsetY;
        e.dataTransfer.setData('x', x);
        e.dataTransfer.setData('y', y);

        setShow(true);

      
            setTimeout(timming, 3000);
    

    }

    function handleDragEnd(e) {

    }

    function handleClick(e) {
        const x = e.nativeEvent.offsetX;
        const y = e.nativeEvent.offsetY;
        console.log(x, y);
    }

    useEffect(() => {
        baobaoContainer.current.style.left = props.x + 'px';
        baobaoContainer.current.style.top = props.y + 'px';
    })

    return (
        <div ref={baobaoContainer} style={{ position: "absolute", top: 0, left: 0, zIndex: 10 }} onDragStart={handleDragStart} onDragEnd={handleDragEnd} onClick={handleClick}>
            <img src={baobaoImg} alt="baobao" />
            {show &&
                <div>
                    <img className="heart" src={heart} style={{ position: "absolute", top: 166, left: 148, width: "10%", transform: "translate(-50% , -48%)" }} alt="heart" />
                    <img className="heart" src={heart} style={{ position: "absolute", top: 151, left: 228, width: "10%", transform: "translate(-50% , -48%)" }} alt="heart" />
                </div>
            }
        </div>
    )
}

export default Baobao;