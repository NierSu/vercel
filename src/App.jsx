import { useEffect, useState } from 'react';

import Baobao from './Baobao';
import Slides from './Slides';
import NeonButton from './NeonButton';
import Line from './Line';

import legs from './img/legs.png';
import dragon from './img/dragon.jpg';
import baobao from './img/baobao.png';

import './App.css';




function App() {
  const [x, setx] = useState(0);
  const [y, sety] = useState(0);
  const [stage, setStage] = useState(0);
  const [legging, setLegging] = useState(false);
  const [one, setOne] = useState(false);
  const [two, setTwo] = useState(false);
  const [three, setThree] = useState(false);

  //ffffffffffffffffffffffffffffffff
  var stage1 = null

  var stage2 = <img className="princess" style={{ position: "absolute", left: "50%", top: "50%", transform: "translate(-50%, -50%)", zIndex: 5 }} src={legs} alt="legs" />

  var stage3 = <div>
    <Baobao x={x} y={y} />
    <img style={{ position: "absolute", left: "50%", top: "50%", transform: "translate(-50%, -50%)", zIndex: 5 }} src={legs} alt="legs" />
  </div>
  //ffffffffffffffffffffffffffffffffffffff

  const [stageHolder, setStageHolder] = useState(stage1);

  useEffect(() => {
    console.log("app update")
    switch (stage) {
      case 1:
        setStageHolder(stage2);
        break;
      case 2:
        setStageHolder(stage3);
        break;
      default:
        console.log('Switch default');
    }
  }, [stage]);



  function handleDragOver(e) {
    e.preventDefault();
  }

  function handleDrop(e) {
    e.preventDefault();
    // console.log(typeof e.clientX, typeof e.dataTransfer.getData('x'));
    setx(e.clientX - parseFloat(e.dataTransfer.getData('x')));
    sety(e.clientY - parseFloat(e.dataTransfer.getData('y')));
  }

  return (
    <div onDragOver={handleDragOver} onDrop={handleDrop}>
      {
        !legging &&
        <Slides setStage={setStage} legging={legging} setLegging={setLegging} showNext={setOne}>
          {(ref) => (
            <p style={{ color: "white", fontSize: 40 }} ref={ref} ></p>
          )}
        </Slides>
      }

      {stageHolder}
      {
        one &&
        <Line rolling={setTwo} customStyle={{ top: "80%", left: '60%', zIndex: 6 }} customImageStyle={{ top: '-30%', left: '-10%', width: "100%" }} image={dragon} setDestroy={setOne} strings={["but she was guarded by 3 dragons in a castle...", 'She waited for her 白马王子 to come to her rescue...']}>
          {(ref, style) => (
            <p style={style} ref={ref}></p>
          )}
        </Line>
      }
      {
        two &&
        <Line rolling={setThree} customStyle={{ top: "10%", left: '60%', zIndex: 6 }} customImageStyle={{ top: '50%', left: '0%', width: "20%" }} image={baobao} setDestroy={setTwo} strings={["此时，在附近的村庄有一名少年。。。", '他就是我们的主人公。。。']}>
          {(ref, style) => (
            <p style={style} ref={ref}></p>
          )}
        </Line>
      }
      {
        three &&
        <NeonButton text="Zhang Baobao" customStyle={{position: "absolute", bottom: "5%", right: "5%"}} />
      }

    </div>
  )
};



export default App;








