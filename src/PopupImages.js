export default function PopupImages(props) {
    return (
        <div className={props.cn}>
            <img style={{ ...props.customStyle, position: "absolute" }} src={props.image} alt="" />
        </div>
    )
}