import React from 'react'
import Parallax from 'parallax-js';

import bgImage from './img/video.mp4';


class Paral extends React.Component {

    constructor(props) {
        super(props);
    }

    componentDidMount() {


        this.parallaxInstance = new Parallax(this.scene, {
            relativeInput: true
        });
        //this.parallaxInstance.enable();
    }

    render() {
        return (
            <div>
                <div id="bgImg" style={{ display: "flex", justifyContent: "center", width: "100vw" }} ref={el => this.scene = el}>

                    <video data-depth="0.5" style={{ width: '120vw' }} src={bgImage} autoPlay loop muted></video>

                </div>
               
            </div>
        );
    }
}

export default Paral;