export default function NeonButton(props) {
    return (
        <div style={props.customStyle}>
            <a className="neon-button" href="#">
                <span></span>
                <span></span>
                <span></span>
                <span></span>
                {props.text}
            </a>        
        </div>)
}